# VGWIN32

![](https://codeberg.org/_-Caleb-_/VGWIN32/raw/branch/main/captura2.PNG)

Vanguard Win32 was developed by _-~Caleb~-_ (05/2023)


This launcher:
---- --------
This program is an effort for recreate the original Vanguard Ace Win95 Launcher with more options like the ship description, etc. This launcher was tested without problems in Windows XP Pro (32 bits), Windows 11 & Linux + Wine.

This launcher was built in apr/may 2023 using an old Pentium 4 - 1.8Ghz - 256 RAM w/ Screenmode 800*600 using Visual Basic 6 (32 bits) for that reason is called "Vanguard Ace Win32"

This launcher use The DOS 1.2 version ditribuited by Midas Interactive and DOSBox preconfigurated for play on modern computers. And after spent years searching for the original Win95 version this is my tribute to this game, my fav vertical shooter ever. The joystick works changing a little bit the dosbox conf (I my case works fine with an Logitech rumble 2).

The game works fine in my P4 using DOSBox (Remember, 1.8Ghz, RAM 256 Mb, 800x600 px)


Difference between versions:
---------- ------- --------
- DOS version (1.0 & 1.2). 
Distribuited as shareware in BBCS, magazines and other media.

- DOS/WINDOWS 95 version (1.2) distribuited by Midas Interactive.
This version includes both versions, DOS and Windows 95 and also include the Win95 Launcher, the Self Demo mode and manuals in several languages.

- DOS (1.2) offered by the author in the official website before the year 2018. This version uses a dosbox 0.71 version preconfigurated to play in modern computers, with a new executable icon but without Windws 95 version and Self Demo.

- DOS (1.2) offered freely for the autor in 2018. Without DOSBox, Windows 95 version and/or Self Demo.
