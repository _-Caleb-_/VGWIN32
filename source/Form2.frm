VERSION 5.00
Begin VB.Form frm_text_reader 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Vanguard Ace Launcher for Windows - Press ESC to exit"
   ClientHeight    =   6360
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   8490
   Icon            =   "Form2.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6360
   ScaleWidth      =   8490
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmd_back 
      Caption         =   "Back"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   495
   End
   Begin VB.PictureBox pc_fra 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000C&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   5640
      MouseIcon       =   "Form2.frx":08CA
      MousePointer    =   99  'Custom
      Picture         =   "Form2.frx":0A1C
      ScaleHeight     =   300
      ScaleWidth      =   480
      TabIndex        =   10
      Top             =   120
      Width           =   480
   End
   Begin VB.TextBox txt_manual_fr 
      BackColor       =   &H00800000&
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00E0E0E0&
      Height          =   975
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   9
      Text            =   "Form2.frx":11DE
      Top             =   3360
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.PictureBox pc_du 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000C&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   4800
      MouseIcon       =   "Form2.frx":1AB6
      MousePointer    =   99  'Custom
      Picture         =   "Form2.frx":1C08
      ScaleHeight     =   300
      ScaleWidth      =   480
      TabIndex        =   8
      Top             =   120
      Width           =   480
   End
   Begin VB.PictureBox pc_ger 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000C&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   3960
      MouseIcon       =   "Form2.frx":23CA
      MousePointer    =   99  'Custom
      Picture         =   "Form2.frx":251C
      ScaleHeight     =   300
      ScaleWidth      =   480
      TabIndex        =   7
      Top             =   120
      Width           =   480
   End
   Begin VB.PictureBox pc_it 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000C&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   3120
      MouseIcon       =   "Form2.frx":2CDE
      MousePointer    =   99  'Custom
      Picture         =   "Form2.frx":2E30
      ScaleHeight     =   300
      ScaleWidth      =   480
      TabIndex        =   6
      Top             =   120
      Width           =   480
   End
   Begin VB.PictureBox pc_sp 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000C&
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   2280
      MouseIcon       =   "Form2.frx":35F2
      MousePointer    =   99  'Custom
      Picture         =   "Form2.frx":3744
      ScaleHeight     =   300
      ScaleWidth      =   480
      TabIndex        =   5
      Top             =   120
      Width           =   480
   End
   Begin VB.TextBox txt_manual_du 
      BackColor       =   &H00800000&
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00E0E0E0&
      Height          =   975
      Left            =   5880
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   4
      Text            =   "Form2.frx":3F06
      Top             =   2160
      Width           =   1815
   End
   Begin VB.TextBox txt_manual_it 
      BackColor       =   &H00800000&
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00E0E0E0&
      Height          =   975
      Left            =   3960
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Text            =   "Form2.frx":469C
      Top             =   2160
      Width           =   1815
   End
   Begin VB.TextBox txt_manual_gr 
      BackColor       =   &H00800000&
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00E0E0E0&
      Height          =   975
      Left            =   2040
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Text            =   "Form2.frx":4E36
      Top             =   2160
      Width           =   1815
   End
   Begin VB.TextBox txt_manual_sp 
      BackColor       =   &H00800000&
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00E0E0E0&
      Height          =   975
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Text            =   "Form2.frx":562B
      Top             =   2160
      Width           =   1695
   End
   Begin VB.TextBox txt_readme 
      BackColor       =   &H00800000&
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00E0E0E0&
      Height          =   1095
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Text            =   "Form2.frx":5E4F
      Top             =   840
      Width           =   1695
   End
End
Attribute VB_Name = "frm_text_reader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmb_idioma_Change()

End Sub

Private Sub cmd_back_Click()
Unload Me
End Sub

Private Sub Form_Load()
txt_readme.Width = Me.Width

End Sub

Private Sub Text1_Change()

End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
If Key = esc Then
    frm_text_reader.Hide
        Else
    End If
End Sub

Private Sub pc_du_Click()
frm_text_reader.txt_manual_du.Visible = True
frm_text_reader.txt_manual_fr.Visible = False
frm_text_reader.txt_manual_gr.Visible = False
frm_text_reader.txt_manual_sp.Visible = False
frm_text_reader.txt_manual_it.Visible = False
frm_text_reader.txt_readme.Visible = False
frm_text_reader.txt_manual_du.Width = frm_text_reader.Width - 50
frm_text_reader.txt_manual_du.Height = frm_text_reader.Height - 800
frm_text_reader.txt_manual_du.Top = 500
frm_text_reader.txt_manual_du.Left = 0
pc_it.BorderStyle = 0
pc_sp.BorderStyle = 0
pc_ger.BorderStyle = 0
pc_du.BorderStyle = 1
pc_fra.BorderStyle = 0
End Sub

Private Sub pc_fra_Click()
frm_text_reader.txt_manual_fr.Visible = True
frm_text_reader.txt_manual_du.Visible = False
frm_text_reader.txt_manual_gr.Visible = False
frm_text_reader.txt_manual_sp.Visible = False
frm_text_reader.txt_manual_it.Visible = False
frm_text_reader.txt_readme.Visible = False
frm_text_reader.txt_manual_fr.Width = frm_text_reader.Width - 50
frm_text_reader.txt_manual_fr.Height = frm_text_reader.Height - 800
frm_text_reader.txt_manual_fr.Top = 500
frm_text_reader.txt_manual_fr.Left = 0
pc_it.BorderStyle = 0
pc_sp.BorderStyle = 0
pc_ger.BorderStyle = 0
pc_du.BorderStyle = 0
pc_fra.BorderStyle = 1
End Sub

Private Sub pc_ger_Click()
frm_text_reader.txt_manual_gr.Visible = True
frm_text_reader.txt_manual_fr.Visible = False
frm_text_reader.txt_manual_du.Visible = False
frm_text_reader.txt_manual_sp.Visible = False
frm_text_reader.txt_manual_it.Visible = False
frm_text_reader.txt_readme.Visible = False
frm_text_reader.txt_manual_gr.Width = frm_text_reader.Width - 50
frm_text_reader.txt_manual_gr.Height = frm_text_reader.Height - 800
frm_text_reader.txt_manual_gr.Top = 500
frm_text_reader.txt_manual_gr.Left = 0
pc_it.BorderStyle = 0
pc_sp.BorderStyle = 0
pc_ger.BorderStyle = 1
pc_du.BorderStyle = 0
pc_fra.BorderStyle = 0
End Sub

Private Sub pc_it_Click()
frm_text_reader.txt_manual_it.Visible = True
frm_text_reader.txt_manual_fr.Visible = False
frm_text_reader.txt_manual_du.Visible = False
frm_text_reader.txt_manual_gr.Visible = False
frm_text_reader.txt_manual_sp.Visible = False
frm_text_reader.txt_readme.Visible = False
frm_text_reader.txt_manual_it.Width = frm_text_reader.Width - 50
frm_text_reader.txt_manual_it.Height = frm_text_reader.Height - 800
frm_text_reader.txt_manual_it.Top = 500
frm_text_reader.txt_manual_it.Left = 0
pc_it.BorderStyle = 1
pc_sp.BorderStyle = 0
pc_ger.BorderStyle = 0
pc_du.BorderStyle = 0
pc_fra.BorderStyle = 0
End Sub

Private Sub pc_sp_Click()
frm_text_reader.txt_manual_sp.Visible = True
frm_text_reader.txt_manual_du.Visible = False
frm_text_reader.txt_manual_gr.Visible = False
frm_text_reader.txt_manual_it.Visible = False
frm_text_reader.txt_manual_fr.Visible = False
frm_text_reader.txt_readme.Visible = False
frm_text_reader.txt_manual_sp.Width = frm_text_reader.Width - 50
frm_text_reader.txt_manual_sp.Height = frm_text_reader.Height - 800
frm_text_reader.txt_manual_sp.Top = 500
frm_text_reader.txt_manual_sp.Left = 0
pc_sp.BorderStyle = 1
pc_it.BorderStyle = 0
pc_ger.BorderStyle = 0
pc_du.BorderStyle = 0
pc_fra.BorderStyle = 0
End Sub

Private Sub txt_readme_KeyPress(KeyAscii As Integer)
    If Key = esc Then
        Unload frm_text_reader
        Else
    End If
End Sub

Private Sub txtabout_KeyPress(KeyAscii As Integer)

End Sub
