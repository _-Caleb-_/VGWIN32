VERSION 5.00
Begin VB.Form frm_shiplist 
   BackColor       =   &H80000007&
   Caption         =   "Ship List"
   ClientHeight    =   5985
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8490
   Icon            =   "Form3.frx":0000
   LinkTopic       =   "Form3"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5985
   ScaleWidth      =   8490
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmd_back 
      Caption         =   "Back"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Width           =   495
   End
   Begin VB.PictureBox Picture5 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   1200
      Left            =   4680
      MouseIcon       =   "Form3.frx":08CA
      MousePointer    =   99  'Custom
      Picture         =   "Form3.frx":0A1C
      ScaleHeight     =   1200
      ScaleWidth      =   2265
      TabIndex        =   5
      Top             =   2400
      Width           =   2265
   End
   Begin VB.PictureBox Picture4 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   1425
      Left            =   2160
      MouseIcon       =   "Form3.frx":209E
      MousePointer    =   99  'Custom
      Picture         =   "Form3.frx":21F0
      ScaleHeight     =   1425
      ScaleWidth      =   2265
      TabIndex        =   4
      Top             =   2280
      Width           =   2265
   End
   Begin VB.PictureBox Picture3 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   1620
      Left            =   5880
      MouseIcon       =   "Form3.frx":3525
      MousePointer    =   99  'Custom
      Picture         =   "Form3.frx":3677
      ScaleHeight     =   1620
      ScaleWidth      =   2265
      TabIndex        =   3
      Top             =   360
      Width           =   2265
   End
   Begin VB.PictureBox Picture2 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   1830
      Left            =   3360
      MouseIcon       =   "Form3.frx":50F5
      MousePointer    =   99  'Custom
      Picture         =   "Form3.frx":5247
      ScaleHeight     =   1830
      ScaleWidth      =   2265
      TabIndex        =   2
      Top             =   120
      Width           =   2265
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      FillColor       =   &H00C0FFFF&
      Height          =   1890
      Left            =   840
      MouseIcon       =   "Form3.frx":6853
      MousePointer    =   99  'Custom
      Picture         =   "Form3.frx":69A5
      ScaleHeight     =   1890
      ScaleWidth      =   2265
      TabIndex        =   1
      Top             =   120
      Width           =   2265
   End
   Begin VB.Label lbl_name 
      AutoSize        =   -1  'True
      BackColor       =   &H8000000B&
      BackStyle       =   0  'Transparent
      Caption         =   "Click on ship for show description"
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   225
      Left            =   120
      TabIndex        =   6
      Top             =   4080
      Width           =   5100
   End
   Begin VB.Label lbl_desc 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   1575
      Left            =   120
      TabIndex        =   0
      Top             =   4320
      Width           =   8175
   End
End
Attribute VB_Name = "frm_shiplist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmd_back_Click()
Unload Me
End Sub

Private Sub Picture1_Click()
lbl_name.Caption = "Charlotte 2000"
lbl_desc.Caption = "This ship packs a punch when you call its backup...Its backup bomber is huge and sends a wallop with tons of hydro-bombs. Any ships within the visible distance of the screen gets wallop by it."
Picture1.Appearance = 0
Picture1.BorderStyle = 1
Picture2.BorderStyle = 0
Picture3.BorderStyle = 0
Picture4.BorderStyle = 0
Picture5.BorderStyle = 0
End Sub

Private Sub Picture2_Click()
lbl_name.Caption = "Chaos EX"
lbl_desc.Caption = "This ship has very very little recharge time before it's ready to fire its next rounds of stinging lasers on its enemies. Equipped with simultaneous missiles that sends enemies flying to limbo."
Picture2.Appearance = 0
Picture2.BorderStyle = 1
Picture1.BorderStyle = 0
Picture3.BorderStyle = 0
Picture4.BorderStyle = 0
Picture5.BorderStyle = 0
End Sub

Private Sub Picture3_Click()
lbl_name.Caption = "Marie FX (Registered Version Only)"
lbl_desc.Caption = "This ship has very powerful pair of boosters and its speed beats all the rest of the ships. Its special bomb is a hybrid of the napalm bomb but able to reach the skies as well."
Picture3.Appearance = 0
Picture3.BorderStyle = 1
Picture1.BorderStyle = 0
Picture2.BorderStyle = 0
Picture4.BorderStyle = 0
Picture5.BorderStyle = 0
End Sub

Private Sub Picture4_Click()
lbl_name.Caption = "Wasp 4000-N2 (Registered Version Only)"
lbl_desc.Caption = "Heavy laser assault. Its laser packs a wallop and its special bomb is a laser with incremental increase in area coverage. Its special lasts the longest among all the ships and packs a painful and burning destruction upon contact with enemy."
Picture4.Appearance = 0
Picture4.BorderStyle = 1
Picture1.BorderStyle = 0
Picture2.BorderStyle = 0
Picture3.BorderStyle = 0
Picture5.BorderStyle = 0
End Sub

Private Sub Picture5_Click()
Picture5.Appearance = 0
Picture5.BorderStyle = 1
Picture1.BorderStyle = 0
Picture2.BorderStyle = 0
Picture3.BorderStyle = 0
Picture4.BorderStyle = 0
lbl_name.Caption = "Phoenix GX (Registered Version Only)"
lbl_desc.Caption = "Originally meant as a physics research aircraft. Modified to be a fighter to aid in the destruction of the 'VISITORS'. Its laser also takes very little time to recharge, just like Chaos-EX. Although its laser is subsequently slower, but packs a punch when it levels ups. Its special ability is its scientific ability to stop time temporary in a particular region that's close to the ship's radius. During the freeze, the Phoenix can take its sweet time walloping the frozen enemies. VERY USEFUL to give bosses more fatal hits."

End Sub
