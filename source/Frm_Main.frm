VERSION 5.00
Begin VB.Form frm_main 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   5280
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8175
   Icon            =   "Frm_Main.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5280
   ScaleWidth      =   8175
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txt_emu 
      Height          =   285
      Left            =   0
      TabIndex        =   20
      Text            =   "Text2"
      Top             =   0
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.CommandButton Command10 
      Caption         =   "Setup DOSBox"
      Height          =   255
      Left            =   5400
      TabIndex        =   8
      ToolTipText     =   "Choose the DOSBox version you will run"
      Top             =   4920
      Width           =   1335
   End
   Begin VB.CommandButton Command9 
      Caption         =   "&About"
      Height          =   255
      Left            =   6840
      TabIndex        =   9
      ToolTipText     =   "About this program"
      Top             =   4920
      Width           =   1215
   End
   Begin VB.CommandButton Command8 
      Caption         =   "&FILE_ID.DIZ"
      Height          =   255
      Left            =   1440
      TabIndex        =   5
      ToolTipText     =   "Show the original FILE_ID.DIZ file"
      Top             =   4920
      Width           =   1215
   End
   Begin VB.CommandButton Command7 
      Caption         =   "&Cheats"
      Height          =   255
      Left            =   4080
      TabIndex        =   7
      ToolTipText     =   "Show the Cheats"
      Top             =   4920
      Width           =   1215
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Ship &List"
      Height          =   255
      Left            =   2760
      TabIndex        =   6
      ToolTipText     =   "Show the Ship List"
      Top             =   4920
      Width           =   1215
   End
   Begin VB.CommandButton Command5 
      Caption         =   "&Manuals"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      ToolTipText     =   "Show multilingual manuals"
      Top             =   4920
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      Height          =   4815
      Left            =   120
      TabIndex        =   10
      Top             =   0
      Width           =   7935
      Begin VB.Frame Frame2 
         BackColor       =   &H00C0C0C0&
         Height          =   1815
         Left            =   4800
         TabIndex        =   14
         Top             =   2760
         Width           =   2895
         Begin VB.CommandButton Command1 
            Caption         =   "&Start Game"
            Height          =   615
            Left            =   120
            TabIndex        =   0
            ToolTipText     =   "Start the game"
            Top             =   240
            Width           =   1215
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Self &Demo"
            Height          =   615
            Left            =   1560
            TabIndex        =   1
            ToolTipText     =   "Start the Self Demo"
            Top             =   240
            Width           =   1215
         End
         Begin VB.CommandButton Command3 
            Caption         =   "&Readme"
            Height          =   615
            Left            =   120
            TabIndex        =   2
            ToolTipText     =   "Show the Readme"
            Top             =   1080
            Width           =   1215
         End
         Begin VB.CommandButton Command4 
            Caption         =   "&Exit"
            Height          =   615
            Left            =   1560
            TabIndex        =   3
            ToolTipText     =   "Exit"
            Top             =   1080
            Width           =   1215
         End
      End
      Begin VB.TextBox Text1 
         Height          =   1335
         Index           =   1
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   13
         Text            =   "Frm_Main.frx":08CA
         ToolTipText     =   "Original Text Info"
         Top             =   3240
         Width           =   4335
      End
      Begin VB.TextBox Text1 
         Height          =   1095
         Index           =   0
         Left            =   4440
         MultiLine       =   -1  'True
         TabIndex        =   12
         Text            =   "Frm_Main.frx":0A01
         ToolTipText     =   "Original Text Info"
         Top             =   1680
         Width           =   3255
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "CLICK START BUTTON TO PLAY"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   240
         Index           =   4
         Left            =   4440
         TabIndex        =   19
         Top             =   360
         Width           =   3195
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "VANGUARD ACE WIN32"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Index           =   1
         Left            =   4440
         TabIndex        =   18
         Top             =   660
         Width           =   3135
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Vertical Madness"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C00000&
         Height          =   255
         Index           =   2
         Left            =   4560
         TabIndex        =   17
         Top             =   960
         Width           =   3015
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Made in Malaysia"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800080&
         Height          =   255
         Index           =   3
         Left            =   4680
         TabIndex        =   16
         Top             =   1260
         Width           =   2895
      End
      Begin VB.Label Label2 
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   1  'Fixed Single
         Height          =   1335
         Index           =   0
         Left            =   4440
         TabIndex        =   15
         Top             =   240
         Width           =   3255
      End
      Begin VB.Image Image1 
         BorderStyle     =   1  'Fixed Single
         Height          =   2835
         Left            =   120
         Picture         =   "Frm_Main.frx":0AB4
         Stretch         =   -1  'True
         ToolTipText     =   "Original and really ugly low-res pixelated original image"
         Top             =   240
         Width           =   4140
      End
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      Height          =   495
      Left            =   3480
      TabIndex        =   11
      Top             =   2280
      Width           =   1215
   End
End
Attribute VB_Name = "frm_main"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    If txt_emu.Text = "db" Then
                Shell "DOSBox.exe -conf va.conf -noconsole"
    Else
        Shell "Dosbox-x.exe -conf vax.conf -fastlaunch -showcycles"
    End If
End Sub

Private Sub Command10_Click()
frm_dosbox.Show
End Sub

Private Sub Command2_Click()
    If txt_emu.Text = "db" Then
        Shell "DOSBox.exe -conf sd.conf -noconsole"
    Else
        Shell "Dosbox-x.exe -conf sdx.conf -fastlaunch -showcycles"
    End If
End Sub

Private Sub Command3_Click()
frm_text_reader.txt_manual_du.Visible = False
frm_text_reader.txt_manual_gr.Visible = False
frm_text_reader.txt_manual_it.Visible = False
frm_text_reader.txt_manual_sp.Visible = False
frm_text_reader.txt_manual_fr.Visible = False
frm_text_reader.txt_readme.Visible = True
frm_text_reader.txt_readme.Width = frm_text_reader.Width
frm_text_reader.txt_readme.Height = frm_text_reader.Height
frm_text_reader.txt_readme.Top = 0
frm_text_reader.txt_readme.Left = 0
frm_text_reader.pc_it.Visible = False
frm_text_reader.pc_du.Visible = False
frm_text_reader.pc_ger.Visible = False
frm_text_reader.pc_sp.Visible = False
frm_text_reader.pc_fra.Visible = False
frm_text_reader.Show
End Sub

Private Sub Command4_Click()
End
End Sub

Private Sub Command5_Click()
frm_text_reader.txt_manual_du.Visible = False
frm_text_reader.txt_manual_gr.Visible = False
frm_text_reader.txt_manual_it.Visible = False
frm_text_reader.txt_manual_sp.Visible = True
frm_text_reader.txt_readme.Visible = False
frm_text_reader.txt_manual_sp.Width = frm_text_reader.Width - 50
frm_text_reader.txt_manual_sp.Height = frm_text_reader.Height - 800
frm_text_reader.txt_manual_sp.Top = 500
frm_text_reader.txt_manual_sp.Left = 0
frm_text_reader.pc_sp.BorderStyle = 1
frm_text_reader.Caption = "Multilingual Manuals"
frm_text_reader.Show
End Sub

Private Sub Command6_Click()
frm_shiplist.Show
End Sub

Private Sub Command7_Click()
Unload frm_fileid
frm_fileid.Command1.Visible = False
frm_fileid.txt_cheats.Top = 0
frm_fileid.txt_cheats.Left = 0
' For GOD MODE
'frm_fileid.txt_cheats.Height = frm_fileid.Height - 900
frm_fileid.txt_cheats.Height = frm_fileid.Height
frm_fileid.txt_cheats.Width = frm_fileid.Width
frm_fileid.txt_cheats.Visible = True
frm_fileid.txt_god.Visible = False
frm_fileid.Caption = "Cheats - Press any key for exit"
frm_fileid.Show
End Sub

Private Sub Command8_Click()
Unload frm_fileid
frm_fileid.Command1.Visible = False
frm_fileid.Text1.Top = 0
frm_fileid.Text1.Left = 0
frm_fileid.Text1.Height = Me.Height
frm_fileid.Text1.Width = Me.Width
frm_fileid.txt_cheats.Visible = False
frm_fileid.txt_god.Visible = False
frm_fileid.Show
End Sub

Private Sub Command9_Click()
frm_about.Show
End Sub

Private Sub Form_Load()
'Variables
frm_main.Height = 5760
txt_emu.Text = "db"
Me.Caption = "Vanguard Ace Win32 - Using DOSBox 0.74"

    If Dir("stderr.txt") <> "" Then
        Kill "stderr.txt"
    Else
    End If
    
    If Dir("stdout.txt") <> "" Then
        Kill "stdout.txt"
    Else
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
End
End Sub
