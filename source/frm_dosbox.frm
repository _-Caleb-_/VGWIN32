VERSION 5.00
Begin VB.Form frm_dosbox 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Emulator"
   ClientHeight    =   2280
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5625
   Icon            =   "frm_dosbox.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2280
   ScaleWidth      =   5625
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Command2 
      Caption         =   "DOSBox-&X"
      Height          =   975
      Left            =   4560
      TabIndex        =   1
      Top             =   1200
      Width           =   975
   End
   Begin VB.TextBox Text2 
      Appearance      =   0  'Flat
      BackColor       =   &H00800000&
      BorderStyle     =   0  'None
      ForeColor       =   &H00C0C0C0&
      Height          =   975
      Left            =   1080
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   3
      Text            =   "frm_dosbox.frx":08CA
      Top             =   1200
      Width           =   3375
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&DOSBox"
      Height          =   975
      Left            =   4560
      TabIndex        =   0
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H00800000&
      BorderStyle     =   0  'None
      ForeColor       =   &H00C0C0C0&
      Height          =   975
      Left            =   1080
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Text            =   "frm_dosbox.frx":0972
      Top             =   120
      Width           =   3375
   End
   Begin VB.Image Image2 
      Height          =   960
      Left            =   120
      Picture         =   "frm_dosbox.frx":09F5
      Stretch         =   -1  'True
      Top             =   120
      Width           =   840
   End
   Begin VB.Image Image1 
      Height          =   915
      Left            =   120
      Picture         =   "frm_dosbox.frx":1397
      Stretch         =   -1  'True
      Top             =   1200
      Width           =   855
   End
End
Attribute VB_Name = "frm_dosbox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
frm_main.txt_emu.Text = "db"
frm_main.Caption = "Vanguard Ace Win32 - Using DOSBox 0.74"
Unload Me
End Sub

Private Sub Command2_Click()
frm_main.txt_emu.Text = "dbx"
frm_main.Caption = "Vanguard Ace Win32 - Using DOSBox-X 2022.12.26"
Unload Me
End Sub
